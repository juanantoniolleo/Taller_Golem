Golem_Network-Desarrollo_CLI-Notas01.txt

* Notas01
    PARA ACCEDER AL CLI DE MODO INTERACTIVO:
golemcli -i

* COMMAND LINE INTERFACE:
https://docs.golem.network/#/Products/Brass-Beta/Command-line-interface

    DENTRO DEL CLI:
    
    - TE INFORMA DEL ESTADO GENERAL DE TU CLIENTE DE GOLEM:
>>status

    - PARA VER LO QUE SE HA GASTADO Y GANADO:
>>incomes all

    - PARA VER DETALLES DE NUESTRA CUENTA:
>>account inform

    - PARA VER EL ESTADO DE LA CUENTA:
>>network status

    - PARA VER LOS NODOS DISPONIBLES A LOS QUE SE PUEDEN MANDAR UNA TAREA: 
>>network show

    - PARA VER LOS AJUSTES DE GOLEM:
>>settings show

    - PARA VER LOS RECURSOS QUE ASIGNAMOS COMO PROVEEDORES:
>>res show

    - PARA VER LAS TAREAS:
>>task show

    - PARA TESTEAR LAS TAREAS:
>>test_task run

    - PARA MOSTRAR LA CACHE:
>>cache show

    - DEBUG RPC (REMOTE PROCEDURE CALLS):
>>debug rpc

    - MUESTRA LOS ENTORNOS:
    - POR EJEMPLO SI SOPORTA GPU, LA POTENCIA QUE APORTA LA CPU, EL NOMBRE DE LAS TAREAS QUE HAY DISPONIBLES
>>envs show

    - PARA CREAR UNA NUEVA TAREA EN GOLEM:
>>golemcli tasks create task.json






