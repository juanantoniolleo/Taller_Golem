# Taller_Golem

### Taller Golem - BlockMAD

ENLACES:
https://golem.network/
https://golem.network/rendering/download
https://docs.golem.network/
https://github.com/golemfactory
https://blog.golemproject.net/

MATERIALES PARA EL TALLER:
https://gitlab.com/juanantoniolleo/Taller_Golem

ATENCION, REPASAR, VIENE BASTANTE INFORMACIÓN:
https://golem.network/crowdfunding/

WHITE PAPER:
https://golem.network/crowdfunding/Golemwhitepaper.pdf

REDDIT:
https://www.reddit.com/r/GolemProject/
https://www.reddit.com/r/GolemProject/comments/8xb9t1/golems_faq_latest_software_update_and_howtos/

KANBAN:
https://trello.com/b/PL4ncR3O/golem-mid-term-goals

PRESS KIT: INCLUYE ESTA PRESENTACIÓN CORTA:
Golem.Network_shortintro.pdf

KNOWLEDGE BASE:
Below we provide the collection of Golem's presentations, documents and other useful materials prepared by us and from external resources.
https://docs.golem.network/#/About/Knowledge-base

ACUERDO DE GOLEM: GRAPHENE NG CON INTEL - SGX

PARA SUBIR UNA TAREA A GOLEM (VIDEOS):
1ª PARTE:
https://www.youtube.com/watch?v=0yV0LNE2prU
2ª PARTE:
https://www.youtube.com/watch?v=BebXK4TQEeU
3ª PARTE:
https://www.youtube.com/watch?v=19viaYSrXOc